//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan  4 12:40:49 2022 by ROOT version 6.25/01
// from TTree ZFinderTree/z finder studies
// found on file: Output.root
//////////////////////////////////////////////////////////

#ifndef ZFinderTree_h
#define ZFinderTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "c++/9/vector"

class ZFinderTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNo;
   UInt_t          lbNo;
   UInt_t          bcid;
   Int_t           nVtx;
   Int_t           nTracks;
   Int_t           nTracksLoose;
   Int_t           nTracksTight;
   Int_t           nTracksEta1Pt200;
   Int_t           nTracksEta1Pt300;
   Int_t           nTracksEta1Pt500;
   Int_t           nTracksEta1Pt700;
   Int_t           nTracksEta1Pt1000;
   Int_t           nTracksEta1Pt200Loose;
   Int_t           nTracksEta1Pt300Loose;
   Int_t           nTracksEta1Pt500Loose;
   Int_t           nTracksEta1Pt700Loose;
   Int_t           nTracksEta1Pt1000Loose;
   Int_t           nTracksEta1Pt200Tight;
   Int_t           nTracksEta1Pt300Tight;
   Int_t           nTracksEta1Pt500Tight;
   Int_t           nTracksEta1Pt700Tight;
   Int_t           nTracksEta1Pt1000Tight;
   Int_t           nSCT;
   Int_t           nPIX;
   Float_t         L1TE;
   Int_t           onlineTracks;
   Float_t         priVtx_x;
   Float_t         priVtx_y;
   Float_t         priVtx_z;
   Float_t         priVtx_weight;
   vector<float>   *zFinder_z;
   vector<float>   *zFinder_weight;
   vector<float>   *zFinder_toolNumber;

   // List of branches
   TBranch        *b_runNo;   //!
   TBranch        *b_lbNo;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nTracksLoose;   //!
   TBranch        *b_nTracksTight;   //!
   TBranch        *b_nTracksEta1Pt200;   //!
   TBranch        *b_nTracksEta1Pt300;   //!
   TBranch        *b_nTracksEta1Pt500;   //!
   TBranch        *b_nTracksEta1Pt700;   //!
   TBranch        *b_nTracksEta1Pt1000;   //!
   TBranch        *b_nTracksEta1Pt200Loose;   //!
   TBranch        *b_nTracksEta1Pt300Loose;   //!
   TBranch        *b_nTracksEta1Pt500Loose;   //!
   TBranch        *b_nTracksEta1Pt700Loose;   //!
   TBranch        *b_nTracksEta1Pt1000Loose;   //!
   TBranch        *b_nTracksEta1Pt200Tight;   //!
   TBranch        *b_nTracksEta1Pt300Tight;   //!
   TBranch        *b_nTracksEta1Pt500Tight;   //!
   TBranch        *b_nTracksEta1Pt700Tight;   //!
   TBranch        *b_nTracksEta1Pt1000Tight;   //!
   TBranch        *b_nSCT;   //!
   TBranch        *b_nPIX;   //!
   TBranch        *b_L1TE;   //!
   TBranch        *b_onlineTracks;   //!
   TBranch        *b_priVtx_x;   //!
   TBranch        *b_priVtx_y;   //!
   TBranch        *b_priVtx_z;   //!
   TBranch        *b_priVtx_weight;   //!
   TBranch        *b_zFinder_z;   //!
   TBranch        *b_zFinder_weight;   //!
   TBranch        *b_zFinder_toolNumber;   //!

   ZFinderTree(TTree *tree=0);
   virtual ~ZFinderTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ZFinderTree_cxx
ZFinderTree::ZFinderTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Output.root");
      }
      f->GetObject("ZFinderTree",tree);

   }
   Init(tree);
}

ZFinderTree::~ZFinderTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ZFinderTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ZFinderTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ZFinderTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   zFinder_z = 0;
   zFinder_weight = 0;
   zFinder_toolNumber = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNo", &runNo, &b_runNo);
   fChain->SetBranchAddress("lbNo", &lbNo, &b_lbNo);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nTracksLoose", &nTracksLoose, &b_nTracksLoose);
   fChain->SetBranchAddress("nTracksTight", &nTracksTight, &b_nTracksTight);
   fChain->SetBranchAddress("nTracksEta1Pt200", &nTracksEta1Pt200, &b_nTracksEta1Pt200);
   fChain->SetBranchAddress("nTracksEta1Pt300", &nTracksEta1Pt300, &b_nTracksEta1Pt300);
   fChain->SetBranchAddress("nTracksEta1Pt500", &nTracksEta1Pt500, &b_nTracksEta1Pt500);
   fChain->SetBranchAddress("nTracksEta1Pt700", &nTracksEta1Pt700, &b_nTracksEta1Pt700);
   fChain->SetBranchAddress("nTracksEta1Pt1000", &nTracksEta1Pt1000, &b_nTracksEta1Pt1000);
   fChain->SetBranchAddress("nTracksEta1Pt200Loose", &nTracksEta1Pt200Loose, &b_nTracksEta1Pt200Loose);
   fChain->SetBranchAddress("nTracksEta1Pt300Loose", &nTracksEta1Pt300Loose, &b_nTracksEta1Pt300Loose);
   fChain->SetBranchAddress("nTracksEta1Pt500Loose", &nTracksEta1Pt500Loose, &b_nTracksEta1Pt500Loose);
   fChain->SetBranchAddress("nTracksEta1Pt700Loose", &nTracksEta1Pt700Loose, &b_nTracksEta1Pt700Loose);
   fChain->SetBranchAddress("nTracksEta1Pt1000Loose", &nTracksEta1Pt1000Loose, &b_nTracksEta1Pt1000Loose);
   fChain->SetBranchAddress("nTracksEta1Pt200Tight", &nTracksEta1Pt200Tight, &b_nTracksEta1Pt200Tight);
   fChain->SetBranchAddress("nTracksEta1Pt300Tight", &nTracksEta1Pt300Tight, &b_nTracksEta1Pt300Tight);
   fChain->SetBranchAddress("nTracksEta1Pt500Tight", &nTracksEta1Pt500Tight, &b_nTracksEta1Pt500Tight);
   fChain->SetBranchAddress("nTracksEta1Pt700Tight", &nTracksEta1Pt700Tight, &b_nTracksEta1Pt700Tight);
   fChain->SetBranchAddress("nTracksEta1Pt1000Tight", &nTracksEta1Pt1000Tight, &b_nTracksEta1Pt1000Tight);
   fChain->SetBranchAddress("nSCT", &nSCT, &b_nSCT);
   fChain->SetBranchAddress("nPIX", &nPIX, &b_nPIX);
   fChain->SetBranchAddress("L1TE", &L1TE, &b_L1TE);
   fChain->SetBranchAddress("onlineTracks", &onlineTracks, &b_onlineTracks);
   fChain->SetBranchAddress("priVtx_x", &priVtx_x, &b_priVtx_x);
   fChain->SetBranchAddress("priVtx_y", &priVtx_y, &b_priVtx_y);
   fChain->SetBranchAddress("priVtx_z", &priVtx_z, &b_priVtx_z);
   fChain->SetBranchAddress("priVtx_weight", &priVtx_weight, &b_priVtx_weight);
   fChain->SetBranchAddress("zFinder_z", &zFinder_z, &b_zFinder_z);
   fChain->SetBranchAddress("zFinder_weight", &zFinder_weight, &b_zFinder_weight);
   fChain->SetBranchAddress("zFinder_toolNumber", &zFinder_toolNumber, &b_zFinder_toolNumber);
   Notify();
}

Bool_t ZFinderTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ZFinderTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ZFinderTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ZFinderTree_cxx
