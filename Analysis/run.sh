# scp -r devmachine:/ATLAS/algilber/zfinder-nt-maker/zfinderntuplemaker/run/Output.root  .
root -b -q driver.C
root -b -q driver1.C
root -b -q driver2.C
root -b -q driver3.C
root -b -q driver4.C
cd plot/
root -b -q plot.C
root -b -q plot2.C
root -b -q plot3.C
root -b -q plot4.C
root -b -q plot5.C
root -b -q nVtx.C

# A very useful line to rename, and even change extension of multiple files. Use % to delete some part
# for i in $( ls c*.pdf ); do mv -- "$i" "_somename_${i%_somename_.extension}_somename_.extension"; done
# BUT for adding name in ending, that minght redundant coz ls name*.extension. Is there any way to avoid it?
for i in $( ls c*.pdf ); do mv -- "$i" "W80Eta1Pt200_${i}"; done
for i in $( ls nVtx*.pdf ); do mv -- "$i" "W80Eta1Pt200_${i}"; done
for i in $( ls nVtx*.root ); do mv -- "$i" "W80Eta1Pt200_${i}"; done
cd VtxCut
for i in $( ls c*.pdf ); do mv -- "$i" "W80Eta1Pt200_${i}"; done
cd ..
cd ..
for i in $( ls nTup*.root ); do mv -- "$i" "W80Eta1Pt200_${i}"; done

# counter=0
# for f in /ATLAS/tbold/DATA/data17_13TeV.00341615.physics_EnhancedBias.merge.AOD-zfinder/*AOD*
# do
#     echo $f
#     athena --filesInput=${f} AthProd/AthProdAlgJobOptions.py > ${counter}athena.log
#     iname=`basename $f`
#     outname=${iname/ESD/NTUP}
#     mv Output.root ${counter}Output.root
#     let 'counter=counter+1'
# done
