#define ZFinderTree_cxx
#include "ZFinderTree.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>

using namespace std;

void ZFinderTree::Loop()
{
//   In a ROOT session, you can do:
//      root> .L ZFinderTree.C
//      root> ZFinderTree t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   TFile* f = TFile::Open("nTup2.root", "RECREATE");
   // vtxfilter = {0 (nothing skipped), 2, 3, 4, >4}
   int vtxfilter = 2;
   if (fChain == 0) return;
   
   const int nTools = 15; 
   double zMin = -300; double zMax = 300; double zBin = 1200;
   double wMin = -2; double wMax = 400; double wBin = 201;
   double wMinOnline = 0; double wMaxOnline = 300; double wBinOnline = 30;
   double trMin = 0; double trMax = 200; double trBin = 40;
   double vtxMin = 0; double vtxMax = 12; double vtxBin = 40;
   double evtMin = 0.5; double evtMax = 1431805.5; int evtBin = 1431805; // 1439, 199, 5, 286361, 1431805
   TH2* h_priVtx_z_Vs_zFinder_z[nTools];            TH2* h_priVtx_z_Vs_zFinder_z_weight[nTools];   TH1* h_diff[nTools];   
   TH1* h_vtx_count[nTools];                        TH2* h_nVtx_zFinder_weight[nTools]; /*??*/       TH2* h_nTracks_zFinder_weight[nTools];
   TH2* h_nTracksLoose_zFinder_weight[nTools];      TH2* h_nTracksTight_zFinder_weight[nTools];    TH1* h_weight_vtx[nTools];
   TEfficiency* e_event[nTools];
   for (int i = 0; i < nTools; i++){
      h_priVtx_z_Vs_zFinder_z[i] = new TH2F(Form("h_priVtx_z_Vs_zFinder_z_%02d",i), ";offline z [mm];online z [mm]", zBin, zMin, zMax, zBin, zMin, zMax);
      h_priVtx_z_Vs_zFinder_z_weight[i] = new TH2F(Form("h_priVtx_z_Vs_zFinder_z_weight%02d",i), ";offline weight;online weight", wBin, wMin, wMax, wBin, wMin, wMax);
      h_nTracks_zFinder_weight[i] = new TH2F(Form("h_nTracks_zFinder_weight%02d",i), ";tracks;online weight", trBin, trMin, trMax, wBinOnline, wMinOnline, wMaxOnline);
      h_nTracksLoose_zFinder_weight[i] = new TH2F(Form("h_nTracksLoose_zFinder_weight%02d",i), ";loose tracks;online weight", trBin, trMin, trMax, wBinOnline, wMinOnline, wMaxOnline);
      h_nTracksTight_zFinder_weight[i] = new TH2F(Form("h_nTracksTight_zFinder_weight%02d",i), ";tight tracks;online weight", trBin, trMin, trMax, wBinOnline, wMinOnline, wMaxOnline);
      h_diff[i] = new TH1F(Form("h_diff%02d",i), ";#delta z [mm];", zBin, zMin, zMax);
      h_vtx_count[i] = new TH1F(Form("h_vtx_count%02d",i), ";N_{vertex};count", 100, 0, 100);
      h_weight_vtx[i] = new TH1F(Form("h_weight_vtx%02d",i), ";weight online;count", 300, 0, 300);
      h_nVtx_zFinder_weight[i] = new TH2F(Form("h_nVtx_zFinder_weight%02d",i), ";Vtx;online weight", vtxBin, vtxMin, vtxMax, wBinOnline, wMinOnline, wMaxOnline);  //???
      e_event[i] = new TEfficiency(Form("e_event%02d",i),"Efficiency;Number of tracks;Efficiency", trBin, trMin, trMax/*evtBin, evtMin, evtMax*/);
      e_event[i]->SetDirectory(gDirectory);
   }
   /*////////////////////////////////////////
   NOTE: priVtx_z->offline, zFinder_z->online
   \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
   TH2* h_nTracks_priVtx_weight = new TH2F("h_nTracks_priVtx_weight", ";track;weigth", trBin, trMin, trMax, wBin, wMin, wMax);
   TH2* h_nTracksLoose_priVtx_weight = new TH2F("h_nTracksLoose_priVtx_weight", ";loose track;weigth", trBin, trMin, trMax, wBin, wMin, wMax);
   TH2* h_nTracksTight_priVtx_weight = new TH2F("h_nTracksTight_priVtx_weight", ";tight track;weigth", trBin, trMin, trMax, wBin, wMin, wMax);
   
   //TH2* h_priVtx_z_Vs_zFinder_z[nTools] = new TH2F("eta_vs_pt_obeserved", "eta vs pt Observed;pt;eta", ptbin, ptR1, ptR2, etabin, etaR1, etaR2);
   
   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      if(priVtx_z==999) continue;
      //cout<<"jentry: "<<jentry<<endl;

      if(vtxfilter == 2 && nVtx != 2) continue;    if(vtxfilter == 3 && nVtx != 3) continue;
      if(vtxfilter == 4 && nVtx != 4) continue;    if(vtxfilter == 5 && nVtx <= 4) continue;
      
      h_nTracks_priVtx_weight->Fill(nTracksEta1Pt200, priVtx_weight);
      h_nTracksLoose_priVtx_weight->Fill(nTracksEta1Pt200Loose, priVtx_weight);
      h_nTracksTight_priVtx_weight->Fill(nTracksEta1Pt200Tight, priVtx_weight);
      
      for ( int toolIndex = 0; toolIndex < nTools; ++toolIndex ) {
         float weightMax = {0};
         float zMax = -999;
         int counter = -1;
         int vtxCount = 0;
         for (auto weight : *zFinder_weight){
            counter++;
            if( zFinder_toolNumber->at(counter) != toolIndex) continue;
            vtxCount++;
            if ( weight > weightMax ) {
               weightMax = weight;
               zMax = zFinder_z->at(counter);
            }
         }
         bool bPassed = weightMax > 80;
         e_event[toolIndex]->Fill(bPassed,nTracksEta1Pt200Loose);
         h_priVtx_z_Vs_zFinder_z[toolIndex]->Fill(priVtx_z, zMax);               h_diff[toolIndex]->Fill(zMax-priVtx_z);
         h_priVtx_z_Vs_zFinder_z_weight[toolIndex]->Fill(priVtx_weight, weightMax);
         if(nTracksEta1Pt200>5) h_nTracks_zFinder_weight[toolIndex]->Fill(nTracksEta1Pt200, weightMax);
         if(nTracksEta1Pt200Loose>5) h_nTracksLoose_zFinder_weight[toolIndex]->Fill(nTracksEta1Pt200Loose, weightMax);
         if(nTracksEta1Pt200Tight>5) h_nTracksTight_zFinder_weight[toolIndex]->Fill(nTracksEta1Pt200Tight, weightMax);
         //if ( vtxCount > 1) cout << "Tool: " << toolIndex << " produced vtx: " << vtxCount << " and max weight: " << weightMax << " z: " << zMax << endl;
         h_vtx_count[toolIndex]->Fill(vtxCount);
         h_weight_vtx[toolIndex]->Fill(nTracksEta1Pt200);
         h_nVtx_zFinder_weight[toolIndex]->Fill(nVtx, weightMax);  // ?????
      }
      /*int k = -1;
      for (auto Vtx_z : *zFinder_z){
         k++;
         if(Vtx_z==-999) continue;
         // 3 alternatives of accessing vector pointer:
         //const float Vtx_z = zFinder_z->at(i);  const float Tool_n = zFinder_toolNumber->at(i);
         //const float Vtx_z = zFinder_z->operator[](i);  const float Tool_n = zFinder_toolNumber->operator[](i);
         
         // const float Vtx_weight = (*zFinder_weight)[k];                     const float Vtx_weight1 = (*zFinder_weight)[k+1];
         // const int Tool_n = (*zFinder_toolNumber)[k];                       const int Tool_n1 = (*zFinder_toolNumber)[k+1];
         
         //for(int nTool =  0; nTool<nTools; nTool++){
         //   if(Tool_n==nTool){
         //      h_priVtx_z_Vs_zFinder_z[nTool]->Fill(priVtx_z, Vtx_z);               h_diff[nTool]->Fill(Vtx_z-priVtx_z); 
         //   }
         //}
         // FIND MAX WEIGHT. This broken shit doesn't work. 
         float weightPar = {0};
         if((*zFinder_toolNumber)[k]==(*zFinder_toolNumber)[k+1]){
            int j=0;
            while ((*zFinder_toolNumber)[k]==(*zFinder_toolNumber)[j+k]){
               if(weightPar<zFinder_weight->at(j+k)) weightPar = zFinder_weight->at(j+k);
               
               cout<<j<<" weightmax: "<<weightPar<<" tool: "<<(*zFinder_toolNumber)[j+k]<<" weight: "<<zFinder_weight->at(j+k)<<endl;
               j++;
            }
            continue;
         }
         //cout<<weightPar<<"; "<<(*zFinder_toolNumber)[k]<<": "<<Tool_n<<endl;
         
         for(int nTool =  0; nTool<nTools; nTool++){
            if(Tool_n==nTool){
               // Debuging
               //cout<<weightPar<<"; "<<Tool_n<<endl;
               h_priVtx_z_Vs_zFinder_z_weight[nTool]->Fill(priVtx_weight, Vtx_weight);//weightPar);//if there's no pile up, weightpar = 0 dude..
               if(nTracksEta1Pt200>5) h_nTracks_zFinder_weight[nTool]->Fill(nTracksEta1Pt200, Vtx_weight);//weightPar);
               if(nTracksEta1Pt200Loose>5) h_nTracksLoose_zFinder_weight[nTool]->Fill(nTracksEta1Pt200Loose, Vtx_weight);//weightPar);
               if(nTracksEta1Pt200Tight>5) h_nTracksTight_zFinder_weight[nTool]->Fill(nTracksEta1Pt200Tight, Vtx_weight);//weightPar);
            }
         }
         // Debuging
         cout <<"weight; "<<(*zFinder_weight)[k]<<". z; "<<Vtx_z<<". Tool; "<<(*zFinder_toolNumber)[k]<<"; "<<endl;
      }*/
      // if (Cut(ientry) < 0) continue;
   }

   f->Write();
   f->Close();
}
