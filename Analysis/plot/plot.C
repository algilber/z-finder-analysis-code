#include "AtlasStyle.C"
static const int nTools = 15;
int const nMinZBin = 5;       int const nPhiBin = 4;

static int l = 2;
void CheckInSequence(const int n, int m, TH1* h[nTools]){
    for (int i = n; i < m+1; i++){
        if(h[i]==NULL) continue;
        h[i]->SetLineColor(l);    h[i]->SetMarkerStyle(l);    h[i]->SetMarkerColor(l);    h[i]->Draw("same, hep");  l++;
    }
}

void CheckSkip(const int n, TH1* h[nTools]){
    int k = n;  int l = 2;
    for (int i = 0; i < nMinZBin; i++){
        if(h[k]==NULL) continue;
        h[k]->SetLineColor(l);    h[k]->SetMarkerStyle(l);    h[k]->SetMarkerColor(l);    h[k]->Draw("same, hep");  k += nPhiBin;  l++;
    }
}

void CheckInSequence2(const int n, int m, TH2* h[nTools]){
    int l = 2;
    for (int i = n; i < m+1; i++){
        if(h[i]==NULL) continue;
        h[i]->SetLineColor(l);    h[i]->SetMarkerStyle(l);    h[i]->SetMarkerColor(l);  l++;    h[i]->Draw("same, hep");
    }
}

void CheckSkip2(const int n, TH2* h[nTools]){
    int k = n;  int l = 2;
    for (int i = 0; i < nMinZBin; i++){
        if(h[k]==NULL) continue;
        h[k]->SetLineColor(l);    h[k]->SetMarkerStyle(l);    h[k]->SetMarkerColor(l);    h[k]->Draw("same, hep");  k += nPhiBin;  l++;
    }
}

#include "AtlasLabels.h"
void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color) 
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}

void plot(){
	gStyle->SetOptStat(0);
    TFile* fIn = TFile::Open("../nTup.root", "READ");
    TFile* fOut = TFile::Open("nVtx.root", "RECREATE");

    TString MinZBinSize[nMinZBin] = {"0.2", "0.5", "1.5", "2.5", "3.5"};
    TString PhiBinSize[nPhiBin] = {"0.20", "0.30", "0.40", "0.50"};

    TH2* h_priVtx_z_Vs_zFinder_z[nTools];   TH2* h_priVtx_z_Vs_zFinder_z_weight[nTools];   TH1* h_diff[nTools];
    TH2* h_nTracks_zFinder_weight[nTools];  TH2* h_nTracksLoose_zFinder_weight[nTools];    TH2* h_nTracksTight_zFinder_weight[nTools];
    TEfficiency* e_event[nTools];
    for (int i = 0; i < nTools; i++){
      h_priVtx_z_Vs_zFinder_z[i] = (TH2F*)fIn->Get(Form("h_priVtx_z_Vs_zFinder_z_%02d",i));
      h_priVtx_z_Vs_zFinder_z_weight[i] = (TH2F*)fIn->Get(Form("h_priVtx_z_Vs_zFinder_z_weight%02d",i));
      h_diff[i] = (TH1F*)fIn->Get(Form("h_diff%02d",i));
      h_nTracks_zFinder_weight[i] = (TH2F*)fIn->Get(Form("h_nTracks_zFinder_weight%02d",i));
      h_nTracksLoose_zFinder_weight[i] = (TH2F*)fIn->Get(Form("h_nTracksLoose_zFinder_weight%02d",i));//  h_nTracksLoose_zFinder_weight[i]->SetLineColor(kRed);
      h_nTracksTight_zFinder_weight[i] = (TH2F*)fIn->Get(Form("h_nTracksTight_zFinder_weight%02d",i));//  h_nTracksTight_zFinder_weight[i]->SetLineColor(kGreen);
      if(h_diff[i]==NULL) continue;
      h_diff[i]->GetXaxis()->SetRangeUser(-9, 9);
      if(i>1){
          h_nTracks_zFinder_weight[i]->GetYaxis()->SetRangeUser(0, 300);     h_nTracksLoose_zFinder_weight[i]->GetYaxis()->SetRangeUser(0, 300);
          h_nTracksTight_zFinder_weight[i]->GetYaxis()->SetRangeUser(0, 300);
      }
      e_event[i]=(TEfficiency*)fIn->Get(Form("e_event%02d",i));
    }
    h_nTracks_zFinder_weight[1]->SetTitle("Default: TripletMode: off, UsePixelOnly: off, #DeltaZ = 0.2 mm, #Delta#phi = 0.20;number of tracks;weight");
    int m = 1;
      for(int l = 0; l < nPhiBin; l++){
        h_nTracks_zFinder_weight[m]->SetTitle(Form("#DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[0].Data(), PhiBinSize[l].Data()));
        h_nTracksLoose_zFinder_weight[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[0].Data(), PhiBinSize[l].Data()));
        h_nTracksTight_zFinder_weight[m]->SetTitle(Form("Tight Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[0].Data(), PhiBinSize[l].Data()));
        e_event[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;Number of tracks;Efficiency", MinZBinSize[0].Data(), PhiBinSize[l].Data()));
        m++;
      }
      for(int l = 1; l < nMinZBin; l++){
        h_nTracks_zFinder_weight[m]->SetTitle(Form("#DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[l].Data(), PhiBinSize[0].Data()));
        h_nTracksLoose_zFinder_weight[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[l].Data(), PhiBinSize[0].Data()));
        h_nTracksTight_zFinder_weight[m]->SetTitle(Form("Tight Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[l].Data(), PhiBinSize[0].Data()));
        e_event[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;Number of tracks;Efficiency", MinZBinSize[l].Data(), PhiBinSize[0].Data()));
        m++;
      }
      for(int l = 1; l < nMinZBin; l++){
        h_nTracks_zFinder_weight[m]->SetTitle(Form("#DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[l].Data(), PhiBinSize[3].Data()));
        h_nTracksLoose_zFinder_weight[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[l].Data(), PhiBinSize[3].Data()));
        h_nTracksTight_zFinder_weight[m]->SetTitle(Form("Tight Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[l].Data(), PhiBinSize[3].Data()));
        e_event[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;Number of tracks;Efficiency", MinZBinSize[l].Data(), PhiBinSize[3].Data()));
        m++;
      }
      for(int l = 1; l < 3; l++){
        h_nTracks_zFinder_weight[m]->SetTitle(Form("#DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[4].Data(), PhiBinSize[l].Data()));
        h_nTracksLoose_zFinder_weight[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[4].Data(), PhiBinSize[l].Data()));
        h_nTracksTight_zFinder_weight[m]->SetTitle(Form("Tight Tracks, #DeltaZ = %s mm, #Delta#phi = %s;number of tracks;weight", MinZBinSize[4].Data(), PhiBinSize[l].Data()));
        e_event[m]->SetTitle(Form("Loose Tracks, #DeltaZ = %s mm, #Delta#phi = %s;Number of tracks;Efficiency", MinZBinSize[4].Data(), PhiBinSize[l].Data()));
        m++;
      }
    
    TH2* h_nTracks_priVtx_weight = (TH2F*)fIn->Get("h_nTracks_priVtx_weight");             h_nTracks_priVtx_weight->SetTitle("All Tracks vs Primary Vertex Weight");
    TH2* h_nTracksLoose_priVtx_weight = (TH2F*)fIn->Get("h_nTracksLoose_priVtx_weight");   h_nTracksLoose_priVtx_weight->SetTitle("Loose Tracks vs Primary Vertex Weight");
    TH2* h_nTracksTight_priVtx_weight = (TH2F*)fIn->Get("h_nTracksTight_priVtx_weight");   h_nTracksTight_priVtx_weight->SetTitle("Tight Tracks vs Primary Vertex Weight");

    // Legends (left, bottom, right, top)
    auto l_minbinz = new TLegend (.75, .7, .9, .9);
    //l_minbinz->SetBorderSize(0);  l_minbinz->SetFillStyle(0);      
    //l_minbinz->AddEntry(h_diff[1], "Default Config.", "lp");    
    l_minbinz->AddEntry(h_diff[0], "Default.", "lp");            l_minbinz->AddEntry(h_diff[1], "#DeltaZ = 0.2 mm", "lp");
    l_minbinz->AddEntry(h_diff[5], "#DeltaZ = 0.5 mm", "lp");    l_minbinz->AddEntry(h_diff[6], "#DeltaZ = 1.5 mm", "lp");
    l_minbinz->AddEntry(h_diff[7], "#DeltaZ = 2.5 mm", "lp");    l_minbinz->AddEntry(h_diff[8], "#DeltaZ = 3.5 mm", "lp");
    
    auto l_phibin = new TLegend (.75, .7, .9, .9);
    l_phibin->SetBorderSize(0);  l_phibin->SetFillStyle(0);      
    l_phibin->AddEntry(h_diff[0], "Default.", "lp");            l_phibin->AddEntry(h_diff[1], "#Delta#phi=0.20", "lp");     l_phibin->AddEntry(h_diff[2], "#Delta#phi=0.30", "lp");
    l_phibin->AddEntry(h_diff[3], "#Delta#phi=0.40", "lp");     l_phibin->AddEntry(h_diff[4], "#Delta#phi=0.50", "lp");
    
    auto l_nTrack_zFinderWeight = new TLegend (.75, .7, .9, .9);
    l_nTrack_zFinderWeight->SetBorderSize(0);  l_nTrack_zFinderWeight->SetFillStyle(0);      
    l_nTrack_zFinderWeight->AddEntry(h_nTracks_zFinder_weight[0], "All tracks", "lp");  l_nTrack_zFinderWeight->AddEntry(h_nTracksLoose_zFinder_weight[3], "Loose tracks", "lp");
    l_nTrack_zFinderWeight->AddEntry(h_nTracksTight_zFinder_weight[0], "Tight tracks", "lp");
    
    TString cName[nTools] = {"Default",
    "MinZBinSize=0.2,PhiBinSize=0.20", "MinZBinSize=0.2,PhiBinSize=0.30", "MinZBinSize=0.2,PhiBinSize=0.40", "MinZBinSize=0.2,PhiBinSize=0.50", 
    "MinZBinSize=0.5,PhiBinSize=0.20", "MinZBinSize=1.5,PhiBinSize=0.20", "MinZBinSize=2.5,PhiBinSize=0.20", "MinZBinSize=3.5,PhiBinSize=0.20", 
    "MinZBinSize=0.5,PhiBinSize=0.50", "MinZBinSize=1.5,PhiBinSize=0.50", "MinZBinSize=2.5,PhiBinSize=0.50", "MinZBinSize=3.5,PhiBinSize=0.50", 
    "MinZBinSize=3.5,PhiBinSize=0.30", "MinZBinSize=3.5,PhiBinSize=0.40"
    };
    TString toolname[nTools] = {"Default: TripletMode: off, UsePixelOnly: off",
    "#DeltaZ = 0.2 mm, #Delta#phi = 0.20.", "#DeltaZ = 0.2 mm, #Delta#phi = 0.30.", "#DeltaZ = 0.2 mm, #Delta#phi = 0.40.", "#DeltaZ = 0.2 mm, #Delta#phi = 0.50.", 
    "#DeltaZ = 0.5 mm, #Delta#phi = 0.20.", "#DeltaZ = 1.5 mm, #Delta#phi = 0.20.", "#DeltaZ = 2.5 mm, #Delta#phi = 0.20.", "#DeltaZ = 3.5 mm, #Delta#phi = 0.20.", 
    "#DeltaZ = 0.5 mm, #Delta#phi = 0.50.", "#DeltaZ = 1.5 mm, #Delta#phi = 0.50.", "#DeltaZ = 2.5 mm, #Delta#phi = 0.50.", "#DeltaZ = 3.5 mm, #Delta#phi = 0.50.", 
    "#DeltaZ = 3.5 mm, #Delta#phi = 0.30.", "#DeltaZ = 3.5 mm, #Delta#phi = 0.40."
    };

    /**********************************************************************************
    0.  TrigZFinder("default") // In large nTuple, this off the chart, so there's only 1 default.
    1.  TrigZFinder("default")

    2. TripletMode=1, MinZBinSize=0.2, PhiBinSize=0.20, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    3. TripletMode=1, MinZBinSize=0.2, PhiBinSize=0.30, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    4. TripletMode=1, MinZBinSize=0.2, PhiBinSize=0.40, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    5. TripletMode=1, MinZBinSize=0.2, PhiBinSize=0.50, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3

    6. TripletMode=1, MinZBinSize=0.5, PhiBinSize=0.20, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    7. TripletMode=1, MinZBinSize=1.5, PhiBinSize=0.20, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    8. TripletMode=1, MinZBinSize=2.5, PhiBinSize=0.20, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    9. TripletMode=1, MinZBinSize=3.5, PhiBinSize=0.20, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3

    10. TripletMode=1, MinZBinSize=0.5, PhiBinSize=0.50, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    11. TripletMode=1, MinZBinSize=1.5, PhiBinSize=0.50, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    12. TripletMode=1, MinZBinSize=2.5, PhiBinSize=0.50, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    13. TripletMode=1, MinZBinSize=3.5, PhiBinSize=0.50, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3

    14. TripletMode=1, MinZBinSize=3.5, PhiBinSize=0.30, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3
    15. TripletMode=1, MinZBinSize=3.5, PhiBinSize=0.40, NumberOfPeaks=5, UseOnlyPixels=True, MaxLayer=3



    **********************************************************************************/
    
    // MinZBin variant
    auto cDifZ_Phi20 = new TCanvas("cDifZ_Phi20", "cDifZ_Phi20");    cDifZ_Phi20->SetLogy();    h_diff[1]->Draw();
    CheckInSequence(5, 8, h_diff);                                   l_minbinz->Draw("same");
    cDifZ_Phi20->SaveAs("cDifZ_Phi20.pdf");                          l = 2;               
    auto cDifZ_Phi50 = new TCanvas("cDifZ_Phi50", "cDifZ_Phi50");    cDifZ_Phi50->SetLogy();    h_diff[1]->Draw();
    CheckInSequence(9, 12, h_diff);                                 l_minbinz->Draw("same");
    cDifZ_Phi50->SaveAs("cDifZ_Phi50.pdf");                          l = 2;
    
    // PhiBin variant
    auto cDifPhi_Z2 = new TCanvas("cDifPhi_Z2", "cDifPhi_Z2");           cDifPhi_Z2->SetLogy();          h_diff[1]->Draw();
    CheckInSequence(1, 4, h_diff);                                       l_phibin->Draw("same");
    cDifPhi_Z2->SaveAs("cDifPhi_Z2.pdf");                                l = 2;
    auto cDifPhi_Z35 = new TCanvas("cDifPhi_Z35", "cDifPhi_Z35");       cDifPhi_Z35->SetLogy();                 h_diff[1]->Draw();
    CheckInSequence(8, 8, h_diff);                                      CheckInSequence(12, 14, h_diff);        l_phibin->Draw("same");
    cDifPhi_Z35->SaveAs("cDifPhi_Z35.pdf");

    auto c5 = new TCanvas("c5", "c5");
    h_nTracks_priVtx_weight->Draw("colz");
    c5->SaveAs("c5.pdf");

    auto c6 = new TCanvas("c6", "c6");
    h_nTracksLoose_priVtx_weight->Draw("colz");
    c6->SaveAs("c6.pdf");

    auto c7 = new TCanvas("c7", "c7");
    h_nTracksTight_priVtx_weight->Draw("colz");
    c7->SaveAs("c7.pdf");

    TCanvas* cEvt[nTools];
    for(int j = 0; j<nTools; j++){
        if(e_event[j]==NULL) continue;
        // e_event[j]->GetXaxis()->SetTitleSize(.05);       e_event[j]->GetYaxis()->SetTitleSize(.05);
        // e_event[j]->GetXaxis()->SetTitleOffset(.9);      e_event[j]->GetYaxis()->SetTitleOffset(.95);
        cEvt[j] = new TCanvas(Form("cEvt%02d", j));
        e_event[j]->Draw();
        ATLASLabel(0.15,0.85,"Internal");
        // cEvt[j]->SetLogx();
        cEvt[j]->SaveAs(Form("cEvt%s.pdf", cName[j].Data()));
    }

    TProfile* p[nTools];///////////////////////////////////////////////
    TCanvas* c[nTools];
    for(int j = 0; j<nTools; j++){
        if(h_nTracks_zFinder_weight[j]==NULL) continue;
        p[j] = h_nTracks_zFinder_weight[j]->ProfileX();////////////////
        //cout<<h_nTracks_zFinder_weight->GetNbinsX()<<endl;
        h_nTracks_zFinder_weight[j]->GetXaxis()->SetTitleSize(.05);       h_nTracks_zFinder_weight[j]->GetYaxis()->SetTitleSize(.05);
        h_nTracks_zFinder_weight[j]->GetXaxis()->SetTitleOffset(.9);     h_nTracks_zFinder_weight[j]->GetYaxis()->SetTitleOffset(.95);
        c[j] = new TCanvas(Form("c%02d", j+3));
        h_nTracks_zFinder_weight[j]->Draw("colz");             //h_nTracksLoose_zFinder_weight[j]->Draw("same");
        // h_nTracksTight_zFinder_weight[j]->Draw("same");   l_nTrack_zFinderWeight->Draw("same");
        p[j]->SetLineColor(kBlack);       p[j]->SetMarkerColor(kBlack);   p[j]->SetMarkerStyle(33);
        p[j]->Draw("same");////////////////////////////////////////////
        ATLASLabel(0.18,0.85,"Internal");
        c[j]->SetLogz();
        c[j]->SaveAs(Form("c%s.pdf", cName[j].Data()));
    }

    //========================================================================================================================Loose
    TProfile* pLoose[nTools];///////////////////////////////////////////////
    TCanvas* cLoose[nTools];
    for(int j = 0; j<nTools; j++){
        if(h_nTracksLoose_zFinder_weight[j]==NULL) continue;
        pLoose[j] = h_nTracksLoose_zFinder_weight[j]->ProfileX();////////////////
        //cout<<h_nTracksLoose_zFinder_weight->GetNbinsX()<<endl;
        h_nTracksLoose_zFinder_weight[j]->GetXaxis()->SetTitleSize(.05);       h_nTracksLoose_zFinder_weight[j]->GetYaxis()->SetTitleSize(.05);
        h_nTracksLoose_zFinder_weight[j]->GetXaxis()->SetTitleOffset(.9);      h_nTracksLoose_zFinder_weight[j]->GetYaxis()->SetTitleOffset(.95);
        cLoose[j] = new TCanvas(Form("cLoose%02d", j));
        h_nTracksLoose_zFinder_weight[j]->Draw("colz");
        pLoose[j]->SetLineColor(kBlack);       pLoose[j]->SetMarkerColor(kBlack);   pLoose[j]->SetMarkerStyle(33);
        pLoose[j]->Draw("same");////////////////////////////////////////////
        ATLASLabel(0.68,0.85,"Internal");
        cLoose[j]->SetLogz();
        cLoose[j]->SaveAs(Form("cLoose%s.pdf", cName[j].Data()));
    }

    //========================================================================================================================Tight
    TProfile* pTight[nTools];///////////////////////////////////////////////
    TCanvas* cTight[nTools];
    for(int j = 0; j<nTools; j++){
        if(h_nTracksTight_zFinder_weight[j]==NULL) continue;
        pTight[j] = h_nTracksTight_zFinder_weight[j]->ProfileX();////////////////
        //cout<<h_nTracksTight_zFinder_weight->GetNbinsX()<<endl;
        h_nTracksTight_zFinder_weight[j]->GetXaxis()->SetTitleSize(.05);       h_nTracksTight_zFinder_weight[j]->GetYaxis()->SetTitleSize(.05);
        h_nTracksTight_zFinder_weight[j]->GetXaxis()->SetTitleOffset(.9);      h_nTracksTight_zFinder_weight[j]->GetYaxis()->SetTitleOffset(.95);
        cTight[j] = new TCanvas(Form("cTight%02d", j));
        h_nTracksTight_zFinder_weight[j]->Draw("colz");
        pTight[j]->SetLineColor(kBlack);       pTight[j]->SetMarkerColor(kBlack);   pTight[j]->SetMarkerStyle(33);
        pTight[j]->Draw("same");////////////////////////////////////////////
        ATLASLabel(0.68,0.85,"Internal");
        cTight[j]->SetLogz();
        cTight[j]->SaveAs(Form("cTight%s.pdf", cName[j].Data()));
    }
    //========================================================================================================================
    
    SetAtlasStyle();

    auto l_Eff = new TLegend (.05, .05, 1., 1.);
    l_Eff->SetBorderSize(0);  l_Eff->SetFillStyle(0);
    auto cEff = new TCanvas("cEff", "cEff");
    e_event[9]->Draw();
    l_Eff->AddEntry(e_event[9], Form("%s", toolname[9].Data()), "lp");
    for (int i = 10; i < 13; i++){
        if(e_event[i]==NULL) continue;
        e_event[i]->SetLineColor(i-8);    e_event[i]->SetMarkerStyle(i-8);    e_event[i]->SetMarkerColor(i-8);    
        e_event[i]->Draw("same");
        l_Eff->AddEntry(e_event[i], Form("%s", toolname[i].Data()), "lp");
    }
    ATLASLabel(0.18,0.88,"Internal");
    cEff->SaveAs("cEff.pdf");
    TCanvas* cEffL = new TCanvas("cEffL", "cEffL");
    l_Eff->Draw();        cEffL->SaveAs("cEffL.pdf");
    
    //========================================================================================================================
    auto l_Effphi = new TLegend (.05, .05, 1., 1.);
    l_Effphi->SetBorderSize(0);  l_Effphi->SetFillStyle(0);
    auto cEffphi = new TCanvas("cEffphi", "cEffphi");
    e_event[8]->Draw();
    l_Effphi->AddEntry(e_event[8], Form("%s", toolname[8].Data()), "lp");
    for (int i = 12; i < 15; i++){
        if(e_event[i]==NULL) continue;
        e_event[i]->SetLineColor(i-10);    e_event[i]->SetMarkerStyle(i-10);    e_event[i]->SetMarkerColor(i-10);    
        e_event[i]->Draw("same");
        if(i!=12) l_Effphi->AddEntry(e_event[i], Form("%s", toolname[i].Data()), "lp");
    }
    l_Effphi->AddEntry(e_event[12], Form("%s", toolname[12].Data()), "lp");
    ATLASLabel(0.18,0.88,"Internal");
    cEffphi->SaveAs("cEffphi.pdf");
    TCanvas* cEffphiL = new TCanvas("cEffphiL", "cEffphiL");
    l_Effphi->Draw();        cEffphiL->SaveAs("cEffphiL.pdf");
    
    //========================================================================================================================
    auto l_p = new TLegend (.05, .05, 1., 1.);
    l_p->SetBorderSize(0);  l_p->SetFillStyle(0);
    
    auto cprof = new TCanvas("cpr", "cpr");
    for(int j = 9; j<nTools; j++){
        p[j]->SetMaximum(300);
        // if(j<9)  { p[j]->SetLineColor(j+1);       p[j]->SetMarkerColor(j+1); }
        // if(j>=9) { p[j]->SetLineColor(j+28);      p[j]->SetMarkerColor(j+28); }
        p[j]->SetLineColor(j-8);       p[j]->SetMarkerColor(j-8);
        
        p[j]->GetXaxis()->SetTitleSize(.05);
        p[j]->GetYaxis()->SetTitleSize(.05);
        p[j]->GetXaxis()->SetTitleOffset(1.05);
        p[j]->GetYaxis()->SetTitleOffset(.88);
        p[j]->SetTitle("; number of tracks (p_{t}>0.2 GeV, |#eta|<1);number of triplets");
        p[j]->SetMarkerStyle(j+25);
        p[j]->Draw("same, HEP");
        l_p->AddEntry(p[j], Form("%s", toolname[j].Data()), "lp");
    }
    ATLASLabel(0.18,0.85,"Internal");
    cprof->SaveAs("cProf_best6.pdf");
    TCanvas* cp = new TCanvas("cp", "cp");
    l_p->Draw();        cp->SaveAs("cP_best6.pdf");
    //========================================================================================================================
    auto cLooseprof = new TCanvas("cLoosepr", "cLoosepr");
    for(int j = 9; j<nTools; j++){
        pLoose[j]->SetMaximum(350);
        // if(j<9)  { pLoose[j]->SetLineColor(j+1);       pLoose[j]->SetMarkerColor(j+1); }
        // if(j>=9) { pLoose[j]->SetLineColor(j+28);      pLoose[j]->SetMarkerColor(j+28); }
        pLoose[j]->SetLineColor(j-8);       pLoose[j]->SetMarkerColor(j-8);
        
        pLoose[j]->GetXaxis()->SetTitleSize(.05);
        pLoose[j]->GetYaxis()->SetTitleSize(.05);
        pLoose[j]->GetXaxis()->SetTitleOffset(1.05);
        pLoose[j]->GetYaxis()->SetTitleOffset(.95);
        pLoose[j]->SetTitle("; number of tracks (p_{t}>0.2 GeV, |#eta|<1);number of triplets");
        pLoose[j]->SetMarkerStyle(j+25);
        pLoose[j]->Draw("same, HEP");
    }
    ATLASLabel(0.18,0.85,"Internal");
    cLooseprof->SaveAs("cLooseProf_best6.pdf");
    TCanvas* cLoosep = new TCanvas("cLoosep", "cLoosep");
    l_p->Draw();        cLoosep->SaveAs("cLooseP_best6.pdf");
    //========================================================================================================================
    auto cTightprof = new TCanvas("cTightpr", "cTightpr");
    for(int j = 9; j<nTools; j++){
        pTight[j]->SetMaximum(350);
        // if(j<9)  { pTight[j]->SetLineColor(j+1);       pTight[j]->SetMarkerColor(j+1); }
        // if(j>=9) { pTight[j]->SetLineColor(j+28);      pTight[j]->SetMarkerColor(j+28); }
        pTight[j]->SetLineColor(j-8);       pTight[j]->SetMarkerColor(j-8);
        
        pTight[j]->GetXaxis()->SetTitleSize(.05);
        pTight[j]->GetYaxis()->SetTitleSize(.05);
        pTight[j]->GetXaxis()->SetTitleOffset(1.05);
        pTight[j]->GetYaxis()->SetTitleOffset(.95);
        pTight[j]->SetTitle("; number of tracks (p_{t}>0.2 GeV, |#eta|<1);number of triplets");
        pTight[j]->SetMarkerStyle(j+25);
        pTight[j]->Draw("same, HEP");
    }
    ATLASLabel(0.18,0.85,"Internal");
    cTightprof->SaveAs("cTightProf_best6.pdf");
    TCanvas* cTightp = new TCanvas("cTightp", "cTightp");
    l_p->Draw();        cTightp->SaveAs("cTightP_best6.pdf");
    
    fOut->Write();
    fOut->Close();
    
}