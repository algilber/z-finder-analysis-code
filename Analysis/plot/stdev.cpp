static const int nTools = 16;
int const nMinZBin = 5;       int const nPhiBin = 4;

void stdev(){
    gStyle->SetOptStat(0);
    TFile* fIn = TFile::Open("../Eta1Pt200_nTup.root", "READ");
    TFile* fOut = TFile::Open("stdev.root", "RECREATE");

    TH2* h_nTracks_zFinder_weight[nTools];  TH2* h_nTracksLoose_zFinder_weight[nTools];     TH2* h_nTracksTight_zFinder_weight[nTools];
    for (int i = 0; i < nTools; i++){
      h_nTracks_zFinder_weight[i] = (TH2F*)fIn->Get(Form("h_nTracks_zFinder_weight%02d",i));
      h_nTracksLoose_zFinder_weight[i] = (TH2F*)fIn->Get(Form("h_nTracksLoose_zFinder_weight%02d",i));//  h_nTracksLoose_zFinder_weight[i]->SetLineColor(kRed);
      h_nTracksTight_zFinder_weight[i] = (TH2F*)fIn->Get(Form("h_nTracksTight_zFinder_weight%02d",i));//  h_nTracksTight_zFinder_weight[i]->SetLineColor(kGreen);
      if(h_nTracks_zFinder_weight[i]==NULL) continue;
      if(i>1){
          h_nTracks_zFinder_weight[i]->GetYaxis()->SetRangeUser(0, 300);     h_nTracksLoose_zFinder_weight[i]->GetYaxis()->SetRangeUser(0, 300);
          h_nTracksTight_zFinder_weight[i]->GetYaxis()->SetRangeUser(0, 300);
      }
    }

    int const nBinTrack = h_nTracks_zFinder_weight[0]->GetNbinsX()+2;
    // cout<<nBinTrack<<endl;
    TH1* h_weight[nTools][nBinTrack];    TH1* h_weightLoose[nTools][nBinTrack];   TH1* h_weightTight[nTools][nBinTrack];
    TH1* h_weightCutLow[nTools];         TH1* h_weightCutLowLoose[nTools];        TH1* h_weightCutLowTight[nTools];
    TH1* h_weightCutHigh[nTools];        TH1* h_weightCutHighLoose[nTools];       TH1* h_weightCutHighTight[nTools];

    for(int i = 0; i<nTools; i++){
        h_weightCutLow[i] = new TH1F(Form("h_weightCutLow%02d",i), ";number of tracks; <weight> - 3#sigma", nBinTrack, 0, 210);
        h_weightCutLowLoose[i] = new TH1F(Form("h_weightCutLowLoose%02d",i), ";number of tracks; <weight> - 3#sigma", nBinTrack, 0, 210);
        h_weightCutLowTight[i] = new TH1F(Form("h_weightCutLowTight%02d",i), ";number of tracks; <weight> - 3#sigma", nBinTrack, 0, 210);
        h_weightCutHigh[i] = new TH1F(Form("h_weightCutHigh%02d",i), ";number of tracks; <weight> + 3#sigma", nBinTrack, 0, 210);
        h_weightCutHighLoose[i] = new TH1F(Form("h_weightCutHighLoose%02d",i), ";number of tracks; <weight> + 3#sigma", nBinTrack, 0, 210);
        h_weightCutHighTight[i] = new TH1F(Form("h_weightCutHighTight%02d",i), ";number of tracks; <weight> + 3#sigma", nBinTrack, 0, 210);
        for(int j = 0; j<nBinTrack; j++){
            h_weight[i][j] = h_nTracks_zFinder_weight[i]->ProjectionY(Form("h_weight%02d%02d", i, j), j, j);
            h_weightLoose[i][j] = h_nTracksLoose_zFinder_weight[i]->ProjectionY(Form("h_weightLoose%02d%02d", i, j), j, j);
            h_weightTight[i][j] = h_nTracksTight_zFinder_weight[i]->ProjectionY(Form("h_weightTight%02d%02d", i, j), j, j);
            if(i==13)cout<<j<<"\t"<<h_weight[i][j]->GetMean()-2*h_weight[i][j]->GetStdDev()<<endl;
            h_weightCutLow[i]->SetBinContent(j, h_weight[i][j]->GetMean()-3*h_weight[i][j]->GetStdDev());
            h_weightCutHigh[i]->SetBinContent(j, h_weight[i][j]->GetMean()+3*h_weight[i][j]->GetStdDev());
            h_weightCutLowLoose[i]->SetBinContent(j, h_weightLoose[i][j]->GetMean()-3*h_weightLoose[i][j]->GetStdDev());
            h_weightCutHighLoose[i]->SetBinContent(j, h_weightLoose[i][j]->GetMean()+3*h_weightLoose[i][j]->GetStdDev());
            h_weightCutLowTight[i]->SetBinContent(j, h_weightTight[i][j]->GetMean()-3*h_weightTight[i][j]->GetStdDev());
            h_weightCutHighTight[i]->SetBinContent(j, h_weightTight[i][j]->GetMean()+3*h_weightTight[i][j]->GetStdDev());
        }
    }
    
    // auto c5 = new TCanvas("c5", "c5");
    if(h_nTracks_zFinder_weight[0]!=NULL) h_nTracks_zFinder_weight[13]->Draw("lego");
    // h_weight[0][0]->Draw();
    
    TString MinZBinSize[nMinZBin] = {"0.2", "0.5", "1.5", "2.5", "3.5"};
    TString PhiBinSize[nPhiBin] = {"0.20", "0.30", "0.40", "0.50"};

    fOut->Write();
    fOut->Close();
}